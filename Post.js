import React from 'react';
import {
  View,
  Text,
} from 'react-native';

export default Post = (props) => {
  const { post } = props;
  return (
    <View style={{ minHeight: 50, borderBottomColor: 'black', borderBottomWidth: 1 }}>
      <Text>{post.text}</Text>
    </View>
  );
}
