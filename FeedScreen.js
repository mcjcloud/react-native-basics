import React, { Component } from 'react';
import {
  Platform,
  Dimensions,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Post from './Post';
import no_post_img from './res/undraw_social_update_puv0.png';

export default class FeedScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: [],
    };
  }

  componentDidMount() {
    // set the createPost call back in the navigation params
    this.props.navigation.setParams({ createPost: this.createPost.bind(this) });
  }

  render() {
    return (
      <View style={[styles.container]}>
        {console}
        {this.state.posts.length <= 0 && (
          <View style={styles.noPostsView}>
            <Image style={styles.image} resizeMode={'contain'} source={no_post_img} />
            <Text style={styles.message}>No posts.</Text>
          </View>
        )}
        {this.state.posts.length > 0 && <FlatList
          style={styles.list}
          data={this.state.posts}
          renderItem={({ item }) => <Post post={item} />}
          keyExtractor={(item, index) => `post_${index}`}
        />}
      </View>
    );
  }

  createPost(post) {
    console.log('post: ', post);
    this.setState({ posts: [...this.state.posts, post] });
  }
}

export const feedNavOptions = ({ navigation }) => ({
  headerTitle: 'Feed',
  headerTintColor: 'white',
  headerStyle: {
    backgroundColor: '#136194'
  },
  headerRight: (
    <TouchableOpacity
      style={styles.addButton}
      onPress={() => {
        navigation.navigate('CreatePostScreen', { createPost: navigation.getParam('createPost') });
      }}
    >
      <Text style={{ fontSize: 32, color: 'white' }}>+</Text>
    </TouchableOpacity>
  ),
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f2f0ea',
    justifyContent: 'center',
  },
  noPostsView: {
    flex: 1,
    justifyContent: 'center',
  },
  list: {
    flex: 1,
  },
  message: {
    textAlign: 'center',
  },
  addButton: {
    backgroundColor: 'transparent',
    marginRight: 10,
  },
  image: {
    width: Dimensions.get('screen').width - 40,
    height: (Dimensions.get('screen').width - 40) * (9 / 16),
    marginLeft: 20,
  }
});
