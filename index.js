/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

// ignore bind warning
console.ignoredYellowBox = ['Warning: bind'];
AppRegistry.registerComponent(appName, () => App);
