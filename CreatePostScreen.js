import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Button,
  Text,
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

export default class CreatePostScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      text: '',
    };
  }

  render() {
    const params = this.props.navigation.state.params;
    return (
      <View style={styles.container}>
        <Text>What's on your mind?</Text>
        <TextInput style={styles.postInput} multiline onChangeText={(text) => this.setState({ text })} />
        <Button title={'Post'} onPress={() => {
          params.createPost({ text: this.state.text });
          this.props.navigation.goBack();
        }} />
      </View>
    );
  }
}

export const createPostNavOptions = ({ navigation }) => ({
  headerTitle: 'Create Post',
  headerTintColor: 'white',
  headerStyle: {
    backgroundColor: '#136194',
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 15,
    backgroundColor: '#f2f0ea',
  },
  postInput: {
    borderColor: 'black',
    borderWidth: 3,
    marginBottom: 15,
  },
});
