/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import { createAppContainer, createStackNavigator } from 'react-navigation';
import FeedScreen, { feedNavOptions } from './FeedScreen';
import CreatePostScreen, { createPostNavOptions } from './CreatePostScreen';

const screens = {
  'Home': {
    screen: FeedScreen,
    navigationOptions: feedNavOptions,
  },
  'CreatePostScreen': {
    screen: CreatePostScreen,
    navigationOptions: createPostNavOptions,
  },
};

const options = {

};

const Navigator = createStackNavigator(screens, { initialRouteName: 'Home' });
export default createAppContainer(Navigator);
